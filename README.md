### idate

A simple package for managing pure date (idate) or pure time (itime) and converting to an from Date.

Both iDate and iTime are basically ints, like 20230712 and 123000 for half 12 on 12th July 2023

Open this project directly in nvim or XCode to edit it,

Do not need to create xcodeproj file. 
