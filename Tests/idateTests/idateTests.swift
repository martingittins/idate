import Foundation
import Testing
@testable import idate

@Test func fromDate() async throws {
    let d = DateComponents(calendar: Calendar.current, year: 2020, month: 4, day: 23).date!
    let ud = iDate(d)
    #expect(ud.value == 20200423)
}

//@Test func fromString() async throws {
//    let ud = iDate("2020-04-23")
//    #expect(ud.value == 20200423)
//}

@Test func fromInt() async throws {
    let ud = iDate(20200423)
    #expect(ud.value == 20200423)
}

@Test func toDate() async throws {
    let ud = iDate(20200423)
    let d = ud.date
    let c = Calendar.current.dateComponents([.year, .month, .day], from: d)
    #expect(c.year == 2020)
    #expect(c.month == 4)
    #expect(c.day == 23)
}

@Test func toString() async throws {
    let ud = iDate(20200423)
    #expect(ud.description == "20200423")
    #expect(ud.isoString == "2020-04-23")
    #expect(ud.string == "23/04/2020")
}

@Test func addDays() async throws {
    let ud = iDate(20200423)
    let ud2 = ud.addDays(days: 1)
    #expect(ud2.value == 20200424)
}

@Test func subtractDays() async throws {
    let ud = iDate(20200423)
    let ud2 = ud.addDays(days: -1)
    #expect(ud2.value == 20200422)
}

@Test func acrossMonthBoundaryDays() async throws {
    let ud = iDate(20240930)
    let ud2 = ud.addDays(days: 1)
    #expect(ud2.value == 20241001)
}

@Test func minusMonthBoundaryDays() async throws {
    let ud = iDate(20241001)
    let ud2 = ud.addDays(days: -2)
    print(ud2.isoString)
    #expect(ud2.value == 20240929)
}

@Test func addYears() async throws {
    let ud = iDate(20200423)
    let ud2 = ud.addYears(years: 1)
    #expect(ud2.value == 20210423)
}

@Test func thatIsoWeekWorks() async throws {
    let j1 = iDate(20200101)
    let j2 = try j1.isoWeekDate()
    #expect(j2.isoString == "2020-W01-3")
    let ud = iDate(20200423)
    let ud2 = try ud.isoWeekDate()
    #expect(ud2.isoString == "2020-W17-4")
//    let y1 = iDate(20241228)
//    for k in 1...8 {
//        let y2 = y1.addDays(days: k)
//        let y3 = try y2.isoWeekDate()
//        print("\(y2.isoString) = \(y3.isoString)")
//    }
//    #expect(j1.isoString == "2020-W01-3")

}

@Test func tryDateFromString() async throws {
    let d1 = iDate.smartDate("20 Aug 2024")
    #expect(d1!.value == 20240820)
    let d2 = iDate.smartDate("20/Aug/2024")
    #expect(d2 == d1)
    let d3 = iDate.smartDate("2024-august-20")
    #expect(d3 == d1)
    let d4 = iDate.smartDate("2024-08-20")
    #expect(d4 == d1)
    let d5 = iDate.smartDate("20th August 2024")
    #expect(d5 == d1)
    let d6 = iDate.smartDate("August 20, 2024")
    #expect(d6 == d1)
    let d7 = iDate.smartDate("2024august20")
    #expect(d7 == d1)
}


