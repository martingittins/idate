//
//  iTime.swift
//  WeatherInput
//
//  Created by Martin Gittins on 21/07/2024.
//

import Foundation

public struct iTime: Comparable, Equatable, Hashable {

    public var value: Int

    public init(_ value: Int) {
        self.value = value
    }

    public init(_ value: Int64) {
        self.value = Int(value)
    }

    public init(hour: Int, minute: Int, second: Int) {
        value = hour * 10000 + minute * 100 + second
    }

    public init(hour: String, minute: String, second: String) {
        if let h = Int(hour), let m = Int(minute), let d = Int(second) {
            value = h * 10000 + m * 100 + d
        }
        else {
            value = 19_700_101
        }
    }

    public init(_ date: Date) {
        let c = Calendar.current.dateComponents([.hour, .minute, .second], from: date)
        value = c.hour! * 10000 + c.minute! * 100 + c.second!
    }

    public var hour: Int { value / 10000 }

    public var minute: Int { (value % 10000) / 100 }

    public var second: Int { value % 100 }

    /// Returns the time as a string in the form hh:mm:ss
    public var string: String { String(format: "%02d:%02d:%4d", hour, minute, second) }

    public static func now() -> iTime {
        iTime(Date.now)
    }

    public func before(_ other: iTime) -> Bool { value < other.value }

    public static func < (lhs: iTime, rhs: iTime) -> Bool {
        lhs.before(rhs)
    }
}
