//
//  iDate.swift
//  WeatherUtil
//
//  Created by Martin Gittins on 09/02/2024.
//

import Foundation

public struct iDate: Comparable, Equatable, Hashable, Sendable {

    public var value: Int

    public static func < (lhs: iDate, rhs: iDate) -> Bool { lhs.value < rhs.value }

    public init(_ value: Int) {
        self.value = value
    }

    public init(_ value: Int64) {
        self.value = Int(value)
    }

    public init(year: Int, month: Int, day: Int) {
        value = year * 10000 + month * 100 + day
    }

    public init(year: String, month: String, day: String) {
        if let y = Int(year), let d = Int(day) {
            let m = iDate.monthName(month)
            value = y * 10000 + m * 100 + d
        }
        else {
            value = 19_700_101
        }
    }

    public init(_ date: Date) {
        let c = Calendar.current.dateComponents([.year, .month, .day], from: date)
        value = c.year! * 10000 + c.month! * 100 + c.day!
    }

    public var year: Int { value / 10000 }

    public var month: Int { (value % 10000) / 100 }

    public var day: Int { value % 100 }

    public var date: Date {
        let c = DateComponents(
            calendar: Calendar.current, timeZone: TimeZone.gmt,
            year: year, month: month, day: day
        )
        return Calendar.current.date(from: c)!
    }

    /// Returns the ISO 8601 week date as a number in the form yyyyww.
    ///  This starts each year on the Monday closest to new years day, so a few days often end up in a different year.
    ///  Every 5 and a bit years there is a leap week and the year has 53 weeks, all others have 52.
    ///  But any date range has a consistent set of weeks.
    public func isoWeekDate() throws -> iWeek {
        let d = date
        let cal = Calendar(identifier: .iso8601)
        var weekday = cal.component(.weekday, from: d) - 1 // cos it starts on a Monday
        if weekday == 0 { weekday = 7 }
        return try iWeek(year: cal.component(.yearForWeekOfYear, from: d),
                     week: cal.component(.weekOfYear, from: d),
                     weekday:weekday)
    }

    /// Returns the date as a string in the form dd/mm/yyyy
    public var string: String { String(format: "%02d/%02d/%4d", day, month, year) }

    /// Returns the date as a string in the form yyyy-mm-dd
    public var isoString: String { String(format: "%4d-%02d-%02d", year, month, day) }

    public var dayOfYear: Int {
        let cal = Calendar.current
        return cal.ordinality(of: .day, in: .year, for: date)!
    }

    public static func now() -> iDate {
        iDate(Date.now)
    }

    public static func fromIsoWeekDate(_ iso: Int) -> iDate {
        let cal = Calendar(identifier: .iso8601)
        let c = cal.date(from: DateComponents(weekOfYear: iso % 100, yearForWeekOfYear: iso / 100))
        let comps = cal.dateComponents(in: .current, from: c!)
        return iDate(year: comps.year!, month: comps.month!, day: comps.day!)
    }

    public static func monthName(_ month: any StringProtocol) -> Int {
        switch month.lowercased() {
        case "january", "jan":
            1
        case "february", "feb":
            2
        case "march", "mar":
            3
        case "april", "apr":
            4
        case "may":
            5
        case "june", "jun":
            6
        case "july", "jul":
            7
        case "august", "aug":
            8
        case "september", "sep":
            9
        case "october", "oct":
            10
        case "november", "nov":
            11
        case "december", "dec":
            12
        default:
            0
        }
    }

    public func before(_ other: iDate) -> Bool { value < other.value }

    public func addDays(days: Int) -> iDate {
        let date = Calendar.current.date(byAdding: .day, value: days, to: date)
        let c = Calendar.current.dateComponents([.year, .month, .day], from: date!)
        return iDate(year: c.year!, month: c.month!, day: c.day!)
    }

    public func addWeeks(weeks: Int) -> iDate {
        let date = Calendar.current.date(byAdding: .day, value: weeks*7, to: date)
        let c = Calendar.current.dateComponents([.year, .month, .day], from: date!)
        return iDate(year: c.year!, month: c.month!, day: c.day!)
    }

    public func addMonths(months: Int) -> iDate {
        let date = Calendar.current.date(byAdding: .month, value: months, to: date)
        let c = Calendar.current.dateComponents([.year, .month, .day], from: date!)
        return iDate(year: c.year!, month: c.month!, day: c.day!)
    }

    public func addYears(years: Int) -> iDate {
        let date = Calendar.current.date(byAdding: .year, value: years, to: date)
        let c = Calendar.current.dateComponents([.year, .month, .day], from: date!)
        return iDate(year: c.year!, month: c.month!, day: c.day!)
    }

    public func daysSince(d: iDate) -> Int {
        Calendar.current.dateComponents([.day], from: d.date, to: date).day!
    }

    public func setDay(_ day: Int) -> iDate {
        iDate(year: year, month: month, day: day)
    }

    public func setMonth(_ month: Int) -> iDate {
        iDate(year: year, month: month, day: day)
    }
}

extension iDate: CustomStringConvertible {
    public var description: String {
        "\(value)"
    }
}
