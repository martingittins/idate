//
//  iWeek.swift
//  idate
//
//  Created by Martin Gittins on 13/01/2025.
//

import Foundation

// Iso week as yyyywwd
// see std iso definition
// since weekday is in range 1-7 - only 1 digits is used, thus year is offset from idate
public struct iWeek: Comparable, Equatable, Hashable {

    public var value: Int

    public static func < (lhs: iWeek, rhs: iWeek) -> Bool { lhs.value < rhs.value }

    public init(_ value: Int) {
        self.value = value
    }

    public init(_ value: Int64) {
        self.value = Int(value)
    }

    public init(year: Int, week: Int, weekday: Int) throws {
        guard week >= 1 && week <= 53 else {
            throw DateError.invalidIsoWeek(week)
        }
        guard weekday >= 1 && weekday <= 7 else {
            throw DateError.invalidIsoWeekday(weekday)
        }
        value = year * 1000 + week * 10 + weekday
    }

    public init(_ date: Date) {
        let c = Calendar(identifier: .iso8601)
        let comps = c.dateComponents([.year, .weekOfYear, .day], from: date)
        value = comps.year! * 1000 + comps.month! * 100 + comps.day!
    }

    public var year: Int { value / 1000 }

    public var week: Int { (value / 10) % 100 }

    public var day: Int { value % 10 }

    public var date: Date {
        let c = Calendar(identifier: .iso8601)
        let comps = DateComponents(
            calendar: c, timeZone: TimeZone.gmt,
            year: year, day: day, weekOfYear: week
        )
        return c.date(from: comps)!
    }

    /// Returns the date as a string in the form yyyy-mm-dd
    public var isoString: String { String(format: "%4d-W%02d-%1d", year, week, day) }


}
