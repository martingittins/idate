//
//  smartDate.swift
//  idate
//
//  Created by Martin Gittins on 21/01/2025.
//
// read date in flexible format - doesn't handle US style month/day/year when month is digits
// i.e. assumes 08/09/1950 is 8th of Sept not 9th of August.

import stringutils

extension iDate {

    public static func smartDate(_ input: String) -> iDate? {
        // assume date thing has three parts separated by space, '/' or '-'
        if input.contains(where: { [" ", "/", "-"].contains($0) }) {
            let parts = input.split(
                whereSeparator: { [" ", "/", "-"].contains($0)
                })

            if parts.count == 3 {
                return dateFromParts(parts)
            }
        }
        let parts2 = splitOnCharacterClass(input)
        switch parts2.count {
            case 1:
                if let d = Int(parts2[0]) {
                    return iDate(d)
                }
            case 3:
                return dateFromParts(parts2)
            default:
                ()
        }
        return nil
    }

    public  static func ordinalInt(_ s: any StringProtocol) -> Int? {
            if s.hasSuffix("th") || s.hasSuffix("st") || s.hasSuffix("nd") {
                return Int(String(s).dropLast(2))
            }
            if s.hasSuffix(",") {
                return Int(String(s).dropLast(1))
            }
            return Int(s)
        }

    public static func dateFromParts(_ parts: [Substring]) -> iDate? {
        // one might be string month, test for starting with letter
        var mloc = -1
        var month = -1
        for k in 0..<3 {
            if parts[k].first?.isLetter ?? false {
                if parts[k].count < 3 || month > -1 {  // only one month is allowed
                    return nil
                }
                mloc = k
                let mstr = parts[k]
                let t = String(mstr)[...2]
                //                let prefix = mstr[mstr.startIndex ..< mstr.index(mstr.startIndex, offsetBy: 3)]
                month = monthName(t)
            }
        }
        if month == -1 {  // no named month - assume day/month/year or year-month-day
                          // month-day-year can't be reliably handled.
            let v1 = Int(parts[0])
            let v2 = Int(parts[1])
            let v3 = Int(parts[2])
            if let v1, let v2, let v3  {
                if v1 > 1000 && v1 < 10000 && v2 >= 1 && v2 <= 12 && v3 >= 1 && v3 <= 31 {
                    return iDate(year: v1, month: v2, day: v3)
                }
                if v3 > 1000 && v3 < 10000 && v2 >= 1 && v2 <= 12 && v1 >= 1 && v1 <= 31 {
                    return iDate(year: v3, month: v2, day: v1)
                }
            }
            return nil
        } else {

            let p1 = mloc == 0 ? 1 : 0
            let p2 = mloc == 2 ? 1 : 2
            let v1 = ordinalInt(parts[p1])
            let v2 = ordinalInt(parts[p2])
            if let v1, let v2  {
                if v1 > 1000 && v1 < 10000 && v2 >= 1 && v2 <= 31 {
                    return iDate(year: v1, month: month, day: v2)
                }
                if v2 > 1000 && v2 < 10000 && v1 >= 1 && v1 <= 31 {
                    return iDate(year: v2, month: month, day: v1)
                }
                // if 2 digit year it likely to be day month year
                if v1 >= 1 && v1 <= 31 && v2 < 100{
                    return iDate(year: v1, month: month, day: v1)
                }
            }
        }
        return nil
    }


}
