//
//  DateError.swift
//  idate
//
//  Created by Martin Gittins on 13/01/2025.
//

public enum DateError: Error {
    case invalidDate(String)
    case invalidIsoWeek(Int)
    case invalidIsoWeekday(Int)

}
